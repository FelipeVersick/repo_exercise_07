﻿using CoAHomeWork;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IDamagable, ILog, IHealable
{
    [SerializeField]
    private int currentHealth;

    [SerializeField]
    private int damage;

    [SerializeField]
    private int heal;

    public void Start()
    {
        Damage(damage);//holt sich damage aus dem editor
        Heal(currentHealth);
    }

    public void Log(object message)
    {
        Debug.Log(message);
    }

    public void Damage(int amount)
    {
        currentHealth -= amount;
        Log("Player got damaged by:" + amount + "Health now by:" + currentHealth);
    }

    public void Heal(int amount)
    {
        currentHealth += amount;
        Log("Player got healed by:" + amount + "Health now by:" + currentHealth);
    }
}
