﻿namespace CoAHomeWork
{
    public interface IHealable
    {
        void Heal(int amount);
    }

}