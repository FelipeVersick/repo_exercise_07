﻿namespace CoAHomeWork
{
    public interface IDamagable
    {
        void Damage(int amount);
    }

}

