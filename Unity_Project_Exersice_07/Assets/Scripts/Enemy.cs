﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CoAHomeWork;

public class Enemy : MonoBehaviour, IDamagable, ILog, IHealable
{
    //[SerializeField]
    private int currentHealth = 20;

    //[SerializeField]
    private int damage = 5;

    //[SerializeField]
    private int heal = 5;

    public void Start()
    {
        Damage(damage + 2);//holt sich damage aus dem editor
        Heal(currentHealth + 2);
    }

    public void Log(object message)
    {
        Debug.Log(message);
    }

    public void Damage(int amount)
    {
        currentHealth -= amount;
        Log("Enemy got damaged by:" + amount + "Health now by:" + currentHealth);
    }

    public void Heal(int amount)
    {
        currentHealth += amount;
        Log("Enemy got healed by:" + amount + "Health now by:" + currentHealth);
    }
}
